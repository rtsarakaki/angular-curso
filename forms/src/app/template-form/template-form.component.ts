import { Component, OnInit } from '@angular/core';

import { UserModel } from './../shared/models/user.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.scss'],
})
export class TemplateFormComponent implements OnInit {
  usuario: UserModel = new UserModel(
    'Ricardo Arakaki',
    'rtsarakaki@gmail.com',
    '09725-780',
    'Rua Rui Barbosa',
    '184',
    'APTO 601C',
    'Jardim Olavo Bilac',
    'São Bernardo do Campo',
    'SP'
  );

  constructor(private http: HttpClient ) {}

  ngOnInit(): void {}

  onSubmit(form: any) {
    console.log(form);
  }

  campoValidacao(campo: any): boolean {
    return <boolean>(!campo.valid && campo.touched);
  }

  formGroupAplicarCSS(campo: any) {
    return {
      'has-error': this.campoValidacao(campo),
      'has-feedback': this.campoValidacao(campo),
    };
  }

  consultaCep(cep: any) {
    var cep = cep.value.replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != '') {
      //Expressão regular para validar o CEP.
      var validacep = /^[0-9]{8}$/;

      //Valida o formato do CEP.
      if (validacep.test(cep)) {
        this.http.get(`https://viacep.com.br/ws/${cep}/json/`)
        .subscribe(data => console.log(data));
      }
    }
  }
}
