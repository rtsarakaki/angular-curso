import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ctrl-campo-error-msg',
  templateUrl: './campo-erro-control.component.html',
  styleUrls: ['./campo-erro-control.component.scss']
})
export class CampoErroControlComponent implements OnInit {

  @Input() visible = true;
  @Input() errorMessage = 'Campo obrigatório.'

  constructor() { }

  ngOnInit(): void {
  }

}
