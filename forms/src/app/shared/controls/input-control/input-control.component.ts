import { CampoErroControlComponent } from './../campo-erro-control/campo-erro-control.component';
import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'ctrl-input',
  templateUrl: './input-control.component.html',
  styleUrls: ['./input-control.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InputControlComponent),
    multi: true
}]
})
export class InputControlComponent
  extends CampoErroControlComponent
  implements OnInit, ControlValueAccessor
{
  @Input() type = 'text';
  @Input() name = '';
  @Input() placeholder = '';
  @Input() label = '';
  @Input() hasError = false;
  @Input() value!: string;
  @Input() required = 'true';

  /**
   * Invoked when the model has been changed
   */
  onChange: (_: any) => void = (_: any) => {};

  /**
   * Invoked when the model has been touched
   */
  onTouched: () => void = () => {};

  /**
   * Method that is invoked on an update of a model.
   */
  updateChanges() {
    this.onChange(this.value);
  }
  /**
   * Writes a new item to the element.
   * @param value the value
   */
  writeValue(value: string): void {
    this.value = value;
    this.updateChanges();
  }

  /**
   * Registers a callback function that should be called when the control's value changes in the UI.
   * @param fn
   */
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  /**
   * Registers a callback function that should be called when the control receives a blur event.
   * @param fn
   */
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  ngOnInit(): void {}

  validate(campo: NgModel): boolean {
    return <boolean>(!campo.valid && campo.touched);
  }

  formGroupApplyCSS(campo: NgModel) {
    return {
      'has-error': this.validate(campo),
      'has-feedback': this.validate(campo),
    };
  }
}
