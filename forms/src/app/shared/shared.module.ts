import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';

import { CampoErroControlComponent } from './controls/campo-erro-control/campo-erro-control.component';
import { InputControlComponent } from './controls/input-control/input-control.component';


@NgModule({
  declarations: [
    CampoErroControlComponent,
    InputControlComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    MatFormFieldModule,
    CampoErroControlComponent,
    InputControlComponent
  ]
})
export class SharedModule { }
