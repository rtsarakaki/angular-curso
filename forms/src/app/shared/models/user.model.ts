export class UserModel {
  constructor(
    public name: string,
    public email: string,
    public cep: string,
    public logardouro: string,
    public numero: string,
    public complemento: string,
    public bairro: string,
    public cidade: string,
    public estado: string
  ) {}
}
