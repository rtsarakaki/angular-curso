import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AlunosDeactivateGuard } from './guards/alunos-deactivate.guard';
import { AlunosGuard } from './guards/alunos.guard';
import { AuthGuard } from './guards/auth.guard';
import { CursosGuard } from './guards/cursos-guard';
import { HomeComponent } from './views/home/home.component';
import { AuthService } from './views/login/auth.service';
import { LoginComponent } from './views/login/login.component';

@NgModule({
  declarations: [AppComponent, HomeComponent, LoginComponent],
  imports: [BrowserModule, FormsModule, AppRoutingModule],
  providers: [AuthService, AuthGuard, CursosGuard, AlunosGuard, AlunosDeactivateGuard],
  bootstrap: [AppComponent],
})
export class AppModule {}
