import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CursoNaoEncontradoComponent } from './curso-nao-encontrado/curso-nao-encontrado.component';
import { CursosDetalheComponent } from './cursos-detalhe/cursos-detalhe.component';
import { CursosResumoComponent } from './cursos-resumo/cursos-resumo.component';
import { CursosRoutingModule } from './cursos-routing.module';
import { CursoService } from './service/curso.service';

@NgModule({
  declarations: [
    CursosResumoComponent,
    CursosDetalheComponent,
    CursoNaoEncontradoComponent,
  ],
  imports: [
    CommonModule,
    CursosRoutingModule
  ],
  providers: [CursoService],
})
export class CursosModule {}
