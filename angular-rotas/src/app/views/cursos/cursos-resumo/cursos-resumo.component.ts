import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { CursoService } from '../service/curso.service';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos-resumo.component.html',
  styleUrls: ['./cursos-resumo.component.scss'],
})
export class CursosResumoComponent implements OnInit {
  cursos!: any[];
  pagina!: number;
  subscricaoQueryParams!: Subscription;

  constructor(
    private cursoService: CursoService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.cursos = this.cursoService.getCursos();
    this.subscricaoQueryParams = this.activatedRoute.queryParams.subscribe((q: any) => {
      this.pagina = q['pagina'];
    })
  }

  ngOnDestroy() {
    this.subscricaoQueryParams.unsubscribe();
  }

  proximaPagina() {
    //this.pagina++;
    this.router.navigate(['/cursos'], { queryParams: {pagina: ++this.pagina }});
  }
}
