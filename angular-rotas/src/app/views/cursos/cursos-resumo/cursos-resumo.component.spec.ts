import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CursosResumoComponent } from './cursos-resumo.component';

describe('CursosComponent', () => {
  let component: CursosResumoComponent;
  let fixture: ComponentFixture<CursosResumoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CursosResumoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CursosResumoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
