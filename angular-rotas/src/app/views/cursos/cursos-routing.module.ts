import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CursoNaoEncontradoComponent } from './curso-nao-encontrado/curso-nao-encontrado.component';
import { CursosDetalheComponent } from './cursos-detalhe/cursos-detalhe.component';
import { CursosResumoComponent } from './cursos-resumo/cursos-resumo.component';


const cursosRoutes: Routes = [
  { path: '', component: CursosResumoComponent },
  { path: 'notFound', component: CursoNaoEncontradoComponent },
  { path: ':id', component: CursosDetalheComponent }
];

@NgModule({
  imports: [RouterModule.forChild(cursosRoutes)],
  exports: [RouterModule]
})
export class CursosRoutingModule { }
