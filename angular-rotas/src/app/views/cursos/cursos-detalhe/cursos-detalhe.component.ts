import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { CursoService } from '../service/curso.service';

@Component({
  selector: 'app-cursos-detalhe',
  templateUrl: './cursos-detalhe.component.html',
  styleUrls: ['./cursos-detalhe.component.scss'],
})
export class CursosDetalheComponent implements OnInit {
  inscricao!: Subscription;
  id!: number;
  curso!: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private cursoService: CursoService
  ) {
    //this.id = route.snapshot.params['id'];
  }

  ngOnInit(): void {
    this.inscricao = this.route.params.subscribe((params: any) => {
      this.id = params['id'];
      this.curso = this.cursoService.getCurso(this.id);

      if (this.curso == null) {
        this.router.navigate(['cursos/notFound']);
      }
    });
  }

  ngOnDestroy() {
    this.inscricao.unsubscribe();
  }
}
