import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { AuthService } from './auth.service';
import { User } from './user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  usuario: User = new User();
  constructor(private authService: AuthService, private router: Router) {
    this.authService.logout();
  }

  ngOnInit(): void {
  }

  login() {
    let autenticado = this.authService.autenticar(this.usuario);
    if (autenticado) {
      this.router.navigate(['']);
    }
  }

}
