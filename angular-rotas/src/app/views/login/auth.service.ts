import { User } from './user';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  autenticado = false;

  constructor() { }

  autenticar(user: User): boolean {
    console.log(user);
    if (user.loginName === 'rtsarakaki@gmail.com' && user.password === 'R1c@rd0t2017') {
      this.autenticado = true;
      return true;
    }
    this.autenticado = false;
    return false;
  }

  logout() {
    this.autenticado = false;
  }
}
