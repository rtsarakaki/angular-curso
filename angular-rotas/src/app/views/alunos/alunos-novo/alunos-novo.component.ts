import { Aluno } from './../model/aluno';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { IFormCanDeactivate } from 'src/app/guards/iform-candeactivate';

import { AlunosService } from './../service/alunos.service';

@Component({
  selector: 'app-alunos-novo',
  templateUrl: './alunos-novo.component.html',
  styleUrls: ['./alunos-novo.component.scss'],
})
export class AlunosNovoComponent implements OnInit, IFormCanDeactivate {
  inscricao!: Subscription;
  id!: number;
  aluno!: Aluno;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private alunosService: AlunosService
  ) {}

  ngOnInit(): void {
    this.inscricao = this.route.data.subscribe( (info) => {
      console.log(info);
      this.aluno = info.dados;
    });
  }

  ngOnDestroy() {
    this.inscricao.unsubscribe();
  }

  podeMudarRota():boolean {

    return confirm('Posso mesmo sair desta página?')

  }
}
