import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { Aluno } from './../model/aluno';
import { AlunosService } from './../service/alunos.service';

@Component({
  selector: 'app-alunos-detalhe',
  templateUrl: './alunos-detalhe.component.html',
  styleUrls: ['./alunos-detalhe.component.scss'],
})
export class AlunosDetalheComponent implements OnInit {
  inscricao!: Subscription;
  aluno!: Aluno;

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.inscricao = this.route.data.subscribe( (info) => {
      this.aluno = info.dados;
    });
  }

  ngOnDestroy() {
    this.inscricao.unsubscribe();
  }

  editarAluno() {
    this.router.navigate(['alunos', this.aluno.id, 'editar']);
  }
}
