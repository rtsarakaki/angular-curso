import { AlunoNovoResolver } from './guards/aluno-novo.resolver';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AlunosDetalheComponent } from './alunos-detalhe/alunos-detalhe.component';
import { AlunosNovoComponent } from './alunos-novo/alunos-novo.component';
import { AlunosResumoComponent } from './alunos-resumo/alunos-resumo.component';
import { AlunosRoutingModule } from './alunos-routing.module';
import { AlunosService } from './service/alunos.service';


@NgModule({
  declarations: [
    AlunosResumoComponent,
    AlunosDetalheComponent,
    AlunosNovoComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    AlunosRoutingModule
  ],
  providers: [AlunosService, AlunoNovoResolver],
})
export class AlunosModule {}
