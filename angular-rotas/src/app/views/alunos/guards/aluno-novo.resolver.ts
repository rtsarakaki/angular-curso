import { AlunosService } from './../service/alunos.service';
import { Aluno } from '../model/aluno';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Resolve,
} from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AlunoNovoResolver implements Resolve<Aluno> {
  constructor(private alunosService: AlunosService) {}

  resolve (
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Aluno> | Promise<Aluno> | Aluno {
    let id = <number> route.params['id'];
    return <Aluno>this.alunosService.getAluno(id);
  }
}
