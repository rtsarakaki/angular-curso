import { Aluno } from './../model/aluno';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AlunosService {
  aluno!: Aluno;

  private alunos: Aluno[] = [
    { id: 1, nome: 'Ricardo', email: 'rtsarakaki@gmail.com' },
    { id: 2, nome: 'Leonardo', email: 'leonardo@gmail.com' },
    { id: 3, nome: 'Luis Fellipe', email: 'lipe@gmail.com' },
  ];
  constructor() {}

  getAlunos(): Aluno[] {
    return this.alunos;
  }

  getAluno(id: number) {
    if (this.aluno?.id != id) {

      console.log('Foi no servidor buscar o aluno.');

      let alunos = this.getAlunos();
      for (let i = 0; i < alunos.length; i++) {
        let aluno = alunos[i];
        if (aluno.id == id) {
          this.aluno = aluno;
          return aluno;
        }
      }

      return null;
    }
    else {
      return this.aluno;
    }
  }
}
