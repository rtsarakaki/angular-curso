import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlunosResumoComponent } from './alunos-resumo.component';

describe('AlunosResumoComponent', () => {
  let component: AlunosResumoComponent;
  let fixture: ComponentFixture<AlunosResumoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlunosResumoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlunosResumoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
