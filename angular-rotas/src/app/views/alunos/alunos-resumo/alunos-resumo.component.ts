import { AlunosService } from './../service/alunos.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alunos-resumo',
  templateUrl: './alunos-resumo.component.html',
  styleUrls: ['./alunos-resumo.component.scss']
})
export class AlunosResumoComponent implements OnInit {

  alunos: any[] = [];

  constructor(private alunosService: AlunosService) { }

  ngOnInit(): void {
    this.alunos = this.alunosService.getAlunos();
  }

}
