import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlunosGuard } from 'src/app/guards/alunos.guard';

import { AlunosDeactivateGuard } from './../../guards/alunos-deactivate.guard';
import { AlunosDetalheComponent } from './alunos-detalhe/alunos-detalhe.component';
import { AlunosNovoComponent } from './alunos-novo/alunos-novo.component';
import { AlunosResumoComponent } from './alunos-resumo/alunos-resumo.component';
import { AlunoNovoResolver } from './guards/aluno-novo.resolver';

const alunosRoutes: Routes = [
  {
    path: '',
    component: AlunosResumoComponent,
    canActivateChild: [AlunosGuard],
    children: [
      { path: 'novo', component: AlunosNovoComponent },
      {
        path: ':id',
        component: AlunosDetalheComponent,
        resolve: { dados: AlunoNovoResolver }
      },
      {
        path: ':id/editar',
        component: AlunosNovoComponent,
        resolve: { dados: AlunoNovoResolver },
        canDeactivate: [AlunosDeactivateGuard],
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(alunosRoutes)],
  exports: [RouterModule],
})
export class AlunosRoutingModule {}
