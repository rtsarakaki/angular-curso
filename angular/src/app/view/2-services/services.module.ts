import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { MaterialModule } from '../../shared/material/material.module';
import { SharedModule } from '../../shared/shared.module';
import { ServicesRoutingModule } from '../2-services/services-routing.module';
import { MoviesCreateComponent } from './aula38/movies-create/movies-create.component';
import { MoviesListComponent } from './aula38/movies-list/movies-list.component';
import { ServiceConsumerComponent } from './aula38/service-consumer/service-consumer.component';

@NgModule({
  declarations: [
    ServiceConsumerComponent,
    MoviesListComponent,
    MoviesCreateComponent,
  ],
  imports: [
    CommonModule,
    ServicesRoutingModule,
    MaterialModule,
    FormsModule,
    SharedModule,
  ],
})
export class ServicesModule {}
