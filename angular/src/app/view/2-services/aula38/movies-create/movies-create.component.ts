import { Component, Input, OnInit, ElementRef, ViewChild } from '@angular/core';

import { MoviesService } from './../../../../service/movies.service';

@Component({
  selector: 'app-movies-create',
  templateUrl: './movies-create.component.html',
  styleUrls: ['./movies-create.component.scss'],
  providers: [MoviesService]
})
export class MoviesCreateComponent implements OnInit {

  @ViewChild('inputMovieName') inputMovieName!: ElementRef;
  movies: string[];

  constructor(private _moviesService: MoviesService) {
    this.movies = this._moviesService.getMovies();
  }

  ngOnInit(): void {}

  adicionarFilme_click() {
    this._moviesService.addMovie(this.inputMovieName.nativeElement.value);
  }
}
