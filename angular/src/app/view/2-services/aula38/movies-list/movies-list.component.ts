import { Component, OnInit } from '@angular/core';
import { MoviesService } from './../../../../service/movies.service';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss'],
  providers: [MoviesService]
})
export class MoviesListComponent implements OnInit {
  movies!: string[]; // = ['De volta para o futuro', 'O resgate do soldado Ryan.', 'Feitiço do Tempo'];
  showMovies: boolean = false;

  constructor(private _moviesService: MoviesService) {

  }

  ngOnInit(): void {
    this.movies = this._moviesService.getMovies();
    this._moviesService.movieCreated.subscribe(
      movie => console.log(movie)
    );
    MoviesService.movieCreatedGlobal.subscribe(
      movie => console.log("global: " + movie)
    );
  }

  changeStateMoviesFrame() {
    this.showMovies = !this.showMovies;
  }
}
