import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { MaterialModule } from '../../shared/material/material.module';
import { SharedModule } from '../../shared/shared.module';
import { PipesUsingComponent } from './aula43/pipes-using/pipes-using.component';
import { PipesRoutingModule } from './pipes-routing.module';

@NgModule({
  declarations: [
    PipesUsingComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    SharedModule,
    PipesRoutingModule,
  ],
})
export class PipesModule {}
