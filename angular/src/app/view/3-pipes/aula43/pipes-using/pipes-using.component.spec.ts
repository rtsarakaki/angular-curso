import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PipesUsingComponent } from './pipes-using.component';

describe('PipesUsingComponent', () => {
  let component: PipesUsingComponent;
  let fixture: ComponentFixture<PipesUsingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PipesUsingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PipesUsingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
