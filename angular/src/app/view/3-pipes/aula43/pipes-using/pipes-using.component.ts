import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';

@Component({
  selector: 'app-pipes-using',
  templateUrl: './pipes-using.component.html',
  styleUrls: ['./pipes-using.component.scss']
})
export class PipesUsingComponent implements OnInit {

  filter: string = '';
  format = 'BRL';

  book: any = {
    title: 'O Grande Conflito',
    rating: 4.92876,
    pages: 314,
    price: 44.99,
    date: new Date(1844, 10, 22),
    url: 'https://www.amazon.com.br/Grande-Conflito-Ellen-G-White/dp/8534514801'
  }

  books: string[] = ['O Desejado de Todas as Nações', 'Caminho a Cristo', 'Conhecer Jesus é Tudo']

  constructor() { }

  ngOnInit(): void {
  }

  getBooks() {
    if (this.books.length === 0 || this.filter === undefined || this.filter.trim() === '') {
      return this.books;
    }

    return this.books.filter((v) => {
      if (v.toLocaleLowerCase().indexOf(this.filter.toLocaleLowerCase()) >= 0) {
        return true;
      }
      return false;
    });
  }

  assyncValue = new Promise((resolve, reject) => {
    setTimeout(() => resolve('Valor assíncrono'), 3000)
  });

  assyncValue2 = interval(3000).subscribe(value => 'Valor assíncrono 2');

}
