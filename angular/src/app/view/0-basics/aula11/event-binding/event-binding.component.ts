import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-binding',
  templateUrl: './event-binding.component.html',
  styleUrls: ['./event-binding.component.scss']
})
export class EventBindingComponent implements OnInit {

  public typedValue = '';
  public savedValue = '';
  public isMouseOver = false;

  constructor() { }

  ngOnInit(): void {
  }

  button_Click() {
    alert('Botão clicado!')
  }

  text_KeyUp(event: KeyboardEvent){
    this.typedValue = (<HTMLInputElement>event.target).value;
  }

  text_Blur(text: string){
    this.savedValue = text;
  }

  label_MouseOverOut() {
    this.isMouseOver = ! this.isMouseOver;
  }
}
