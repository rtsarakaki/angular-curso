import { LifeCycleConsumerComponent } from './aula15/life-cycle-consumer/life-cycle-consumer.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DataBindingStyleComponent } from './aula10/data-binding-style/data-binding-style.component';
import { EventBindingComponent } from './aula11/event-binding/event-binding.component';
import { TwoWayDataBindingComponent } from './aula12/two-way-data-binding/two-way-data-binding.component';
import { CreateComponentConsumerComponent } from './aula13/create-component-consumer/create-component-consumer.component';
import { DataBindingComponent } from './aula9/data-binding/data-binding.component';
import { OutputPropertyConsumerComponent } from './aula14/output-property-consumer/output-property-consumer.component';

const routes: Routes = [
  { path: 'aula9', component: DataBindingComponent },
  { path: 'aula10', component: DataBindingStyleComponent },
  { path: 'aula11', component: EventBindingComponent },
  { path: 'aula12', component: TwoWayDataBindingComponent },
  { path: 'aula13', component: CreateComponentConsumerComponent },
  { path: 'aula14', component: OutputPropertyConsumerComponent },
  { path: 'aula15', component: LifeCycleConsumerComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BasicsRoutingModule { }
