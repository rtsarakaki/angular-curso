import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeCycleConsumerComponent } from './life-cycle-consumer.component';

describe('LifeCycleConsumerComponent', () => {
  let component: LifeCycleConsumerComponent;
  let fixture: ComponentFixture<LifeCycleConsumerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LifeCycleConsumerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeCycleConsumerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
