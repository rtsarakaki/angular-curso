import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-life-cycle-consumer',
  templateUrl: './life-cycle-consumer.component.html',
  styleUrls: ['./life-cycle-consumer.component.scss']
})
export class LifeCycleConsumerComponent implements OnInit {

  public value: string = '10';

  constructor() { }

  ngOnInit(): void {
  }
}
