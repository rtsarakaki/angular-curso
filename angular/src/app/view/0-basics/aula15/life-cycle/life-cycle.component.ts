import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-life-cycle',
  templateUrl: './life-cycle.component.html',
  styleUrls: ['./life-cycle.component.scss'],
})
export class LifeCycleComponent implements OnInit {

  @Input() public value = '';

  //EVENTO IMPORTANTE
  constructor() {
    this.write('constructor');
  }

  //EVENTO IMPORTANTE
  ngOnInit(): void {
    this.write('ngOnInit');
  }

  //EVENTO IMPORTANTE
  ngOnChanges() {
    this.write('ngOnChanges');
   }

  ngDoCheck() {
    this.write('ngDoCheck');
  }

  ngAfterContentInit() {
    this.write('ngAfterContentInit');
  }

  ngAfterContentChecked() {
    this.write('ngAfterContentChecked');
  }

  ngAfterViewInit() {
    this.write('ngAfterViewInit');
  }

  ngAfterViewChecked() {
    this.write('ngAfterViewChecked');
  }

  ngOnDestroy() {
    this.write('ngOnDestroy');
  }

  write(methodName: string) {
    console.log(methodName);
  }
}
