import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateComponentConsumerComponent } from './create-component-consumer.component';

describe('CreateComponentConsumerComponent', () => {
  let component: CreateComponentConsumerComponent;
  let fixture: ComponentFixture<CreateComponentConsumerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateComponentConsumerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateComponentConsumerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
