import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'header-component',
  templateUrl: './create-component.component.html',
  styleUrls: ['./create-component.component.scss']
})
export class CreateComponentComponent implements OnInit {

  @Input('title')  title: string = 'Titulo da página';

  constructor() { }

  ngOnInit(): void {
  }

}
