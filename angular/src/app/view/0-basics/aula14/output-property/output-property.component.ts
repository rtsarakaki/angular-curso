import {
  Component,
  Input,
  OnInit,
  EventEmitter,
  Output,
  ViewChild,
  ElementRef,
} from '@angular/core';

@Component({
  selector: 'app-output-property',
  templateUrl: './output-property.component.html',
  styleUrls: ['./output-property.component.scss'],
})
export class OutputPropertyComponent implements OnInit {
  @Output() onChange = new EventEmitter();
  @ViewChild('inputVisor') inputVisor!: ElementRef;

  constructor() {}

  private _value: number = 0;

  ngOnInit(): void {
  }

  @Input()
  set value(val: number) {
    this._value = val;
    //this.inputVisor.nativeElement.value = val;
  }
  get value(): number {
    return this._value;
    //return this.inputVisor.nativeElement.value;
  }

  increase() {
    this.value++;
    this.onChange.emit({ newValue: this.value, type: 'increase' });
    console.log(this.inputVisor.nativeElement);
  }

  decrease() {
    if (this.value > 0) {
      this.value--;
      this.onChange.emit({
        newValue: this.value,
        type: 'decrease',
      });
    }
  }
}
