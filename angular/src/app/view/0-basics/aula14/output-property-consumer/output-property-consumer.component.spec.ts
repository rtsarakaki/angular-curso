import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputPropertyConsumerComponent } from './output-property-consumer.component';

describe('OutputPropertyConsumerComponent', () => {
  let component: OutputPropertyConsumerComponent;
  let fixture: ComponentFixture<OutputPropertyConsumerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OutputPropertyConsumerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputPropertyConsumerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
