import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-output-property-consumer',
  templateUrl: './output-property-consumer.component.html',
  styleUrls: ['./output-property-consumer.component.scss']
})
export class OutputPropertyConsumerComponent implements OnInit {

  value = 0;
  type: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  output_onChange(event: any) {
    this.value = event.newValue;
    this.type = event.type;
  }

}
