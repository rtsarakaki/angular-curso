import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MaterialModule } from '../../shared/material/material.module';
import { DataBindingStyleComponent } from './aula10/data-binding-style/data-binding-style.component';
import { DataBindingComponent } from './aula9/data-binding/data-binding.component';
import { BasicsRoutingModule } from './basics-routing.module';
import { EventBindingComponent } from './aula11/event-binding/event-binding.component';
import { TwoWayDataBindingComponent } from './aula12/two-way-data-binding/two-way-data-binding.component';

import { FormsModule } from '@angular/forms';
import { CreateComponentComponent } from './aula13/create-component/create-component.component';
import { CreateComponentConsumerComponent } from './aula13/create-component-consumer/create-component-consumer.component';
import { OutputPropertyComponent } from './aula14/output-property/output-property.component';
import { OutputPropertyConsumerComponent } from './aula14/output-property-consumer/output-property-consumer.component';
import { LifeCycleConsumerComponent } from './aula15/life-cycle-consumer/life-cycle-consumer.component';
import { LifeCycleComponent } from './aula15/life-cycle/life-cycle.component';

@NgModule({
  declarations: [
    DataBindingComponent,
    DataBindingStyleComponent,
    EventBindingComponent,
    TwoWayDataBindingComponent,
    CreateComponentComponent,
    CreateComponentConsumerComponent,
    OutputPropertyComponent,
    OutputPropertyConsumerComponent,
    LifeCycleConsumerComponent,
    LifeCycleComponent
  ],
  imports: [
    CommonModule,
    BasicsRoutingModule,
    MaterialModule,
    FormsModule
  ]
})
export class BasicsModule { }
