import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PlainTextConsumerComponent } from './plain-text/plain-text-consumer/plain-text-consumer.component';

const routes: Routes = [
  { path: 'alert', component: PlainTextConsumerComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FrameworkRoutingModule {}
