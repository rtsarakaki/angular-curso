import { FormControl } from '@angular/forms';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-input',
  template: `
    <input [formControl]="control" />
    {{ control.value }}
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputComponent  {
  control = new FormControl();
}
