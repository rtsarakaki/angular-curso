import { Component, Injector, OnInit } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FormControl } from '@angular/forms';
import { AlertComponent } from '../alert/alert.component';

@Component({
  selector: 'app-plain-text-consumer',
  templateUrl: './plain-text-consumer.component.html',
  styleUrls: ['./plain-text-consumer.component.scss']
})
export class PlainTextConsumerComponent implements OnInit {
  control = new FormControl('<app-alert>This is neat</app-alert>');
  textToHtml = '';

  code = `
    <input type="text">
    <button>Teste</button>
    <h1>dinamico</h1>
  `;

  codeShow = '';

  stringToHTML(str: string): HTMLElement {
    var parser = new DOMParser();
    var doc = parser.parseFromString(str, 'text/html');
    return doc.body;
  };

  constructor(private injector: Injector) {
    console.log(this.stringToHTML(this.code));
  }

  ngOnInit() {
    const element = createCustomElement(AlertComponent, { injector: this.injector });
    customElements.define('app-alert', element);
  }

}
