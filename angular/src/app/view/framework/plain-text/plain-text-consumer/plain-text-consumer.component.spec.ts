import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlainTextConsumerComponent } from './plain-text-consumer.component';

describe('PlainTextConsumerComponent', () => {
  let component: PlainTextConsumerComponent;
  let fixture: ComponentFixture<PlainTextConsumerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlainTextConsumerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlainTextConsumerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
