import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from '../../shared/material/material.module';
import { SharedModule } from '../../shared/shared.module';
import { FrameworkRoutingModule } from './framework-routing.module';
import { AlertComponent } from './plain-text/alert/alert.component';
import { PlainTextConsumerComponent } from './plain-text/plain-text-consumer/plain-text-consumer.component';
import { InputComponent } from './plain-text/input/input.component';

@NgModule({
  declarations: [AlertComponent, PlainTextConsumerComponent, InputComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    FrameworkRoutingModule,
  ],
})
export class FrameworkModule {}
