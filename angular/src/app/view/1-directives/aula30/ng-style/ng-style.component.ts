import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-style',
  templateUrl: './ng-style.component.html',
  styleUrls: ['./ng-style.component.scss']
})
export class NgStyleComponent implements OnInit {

  active = true;
  fontSize = 25

  constructor() { }

  ngOnInit(): void {
  }

  button_click() {
    this.active = !this.active;
  }

}
