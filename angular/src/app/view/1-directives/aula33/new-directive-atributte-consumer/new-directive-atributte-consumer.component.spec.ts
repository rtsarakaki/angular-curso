import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewDirectiveAtributteConsumerComponent } from './new-directive-atributte-consumer.component';

describe('NewDirectiveAtributteConsumerComponent', () => {
  let component: NewDirectiveAtributteConsumerComponent;
  let fixture: ComponentFixture<NewDirectiveAtributteConsumerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewDirectiveAtributteConsumerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewDirectiveAtributteConsumerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
