import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appNewDirectiveAtributteConsumer]'
})
export class NewDirectiveAtributteDirective {

  constructor(_elementRef: ElementRef) {
    _elementRef.nativeElement.style.backgroundColor = 'yellow';
  }

}
