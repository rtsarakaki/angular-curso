import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NgifComponent } from './aula25/ngif/ngif.component';
import { NgSwitchComponent } from './aula26/ngswitch/ngswitch.component';
import { NgForComponent } from './aula27/ngfor/ngfor.component';
import { NgClassComponent } from './aula29/ng-class/ng-class.component';
import { NgStyleComponent } from './aula30/ng-style/ng-style.component';
import { ElvisOperatorComponent } from './aula31/elvis-operator/elvis-operator.component';
import { NgContentConsumerComponent } from './aula32/ng-content-consumer/ng-content-consumer.component';
import {
  NewDirectiveAtributteConsumerComponent,
} from './aula33/new-directive-atributte-consumer/new-directive-atributte-consumer.component';
import {
  StructureDirectiveConsumerComponent,
} from './aula36/structure-directive-consumer/structure-directive-consumer.component';

const routes: Routes = [
  { path: 'aula25', component: NgifComponent },
  { path: 'aula26', component: NgSwitchComponent },
  { path: 'aula27', component: NgForComponent },
  { path: 'aula29', component: NgClassComponent },
  { path: 'aula30', component: NgStyleComponent },
  { path: 'aula31', component: ElvisOperatorComponent },
  { path: 'aula32', component: NgContentConsumerComponent },
  { path: 'aula33', component: NewDirectiveAtributteConsumerComponent },
  { path: 'aula36', component: StructureDirectiveConsumerComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DirectivesRoutingModule {}
