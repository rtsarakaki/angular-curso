import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StructureDirectiveConsumerComponent } from './structure-directive-consumer.component';

describe('StructureDirectiveConsumerComponent', () => {
  let component: StructureDirectiveConsumerComponent;
  let fixture: ComponentFixture<StructureDirectiveConsumerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StructureDirectiveConsumerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StructureDirectiveConsumerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
