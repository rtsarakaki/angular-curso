import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-structure-directive-consumer',
  templateUrl: './structure-directive-consumer.component.html',
  styleUrls: ['./structure-directive-consumer.component.scss']
})
export class StructureDirectiveConsumerComponent implements OnInit {

  movies: string[] = ['De volta para o futuro', 'O resgate do soldado Ryan.', 'Feitiço do Tempo'];
  showMovies: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  changeStateMoviesFrame() {
    this.showMovies = !this.showMovies;
  }
}
