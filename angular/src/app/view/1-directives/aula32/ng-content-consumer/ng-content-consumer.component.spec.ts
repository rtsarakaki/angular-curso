import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgContentConsumerComponent } from './ng-content-consumer.component';

describe('NgContentConsumerComponent', () => {
  let component: NgContentConsumerComponent;
  let fixture: ComponentFixture<NgContentConsumerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgContentConsumerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgContentConsumerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
