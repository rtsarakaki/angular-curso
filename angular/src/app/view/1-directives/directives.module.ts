import { SharedModule } from './../../shared/shared.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { MaterialModule } from './../../shared/material/material.module';
import { DirectivesRoutingModule } from './../1-directives/directives-routing.module';
import { NgifComponent } from './aula25/ngif/ngif.component';
import { NgSwitchComponent } from './aula26/ngswitch/ngswitch.component';
import { NgForComponent } from './aula27/ngfor/ngfor.component';
import { NgClassComponent } from './aula29/ng-class/ng-class.component';
import { NgStyleComponent } from './aula30/ng-style/ng-style.component';
import { ElvisOperatorComponent } from './aula31/elvis-operator/elvis-operator.component';
import { NgContentConsumerComponent } from './aula32/ng-content-consumer/ng-content-consumer.component';
import { NgContentComponent } from './aula32/ng-content/ng-content.component';
import { NewDirectiveAtributteDirective } from './aula33/new-directive-atributte.directive';
import { NewDirectiveAtributteConsumerComponent } from './aula33/new-directive-atributte-consumer/new-directive-atributte-consumer.component';
import { StructureDirectiveConsumerComponent } from './aula36/structure-directive-consumer/structure-directive-consumer.component';

@NgModule({
  declarations: [
    NgifComponent,
    NgSwitchComponent,
    NgForComponent,
    NgClassComponent,
    NgStyleComponent,
    ElvisOperatorComponent,
    NgContentComponent,
    NgContentConsumerComponent,
    NewDirectiveAtributteConsumerComponent,
    NewDirectiveAtributteDirective,
    StructureDirectiveConsumerComponent,
  ],
  imports: [
    CommonModule,
    DirectivesRoutingModule,
    MaterialModule,
    FormsModule,
    SharedModule,
  ],
})
export class DirectivesModule {}
