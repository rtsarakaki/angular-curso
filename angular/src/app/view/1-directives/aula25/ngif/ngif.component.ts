import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ngif',
  templateUrl: './ngif.component.html',
  styleUrls: ['./ngif.component.scss']
})
export class NgifComponent implements OnInit {

  movies: string[] = ['De volta para o futuro'];
  showMovies: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  changeStateMoviesFrame() {
    this.showMovies = !this.showMovies;
  }

}
