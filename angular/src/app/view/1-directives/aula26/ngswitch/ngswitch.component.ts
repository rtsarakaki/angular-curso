import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-switch',
  templateUrl: './ngswitch.component.html',
  styleUrls: ['./ngswitch.component.scss']
})
export class NgSwitchComponent implements OnInit {

  tab = 'home';

  constructor() { }

  ngOnInit(): void {
  }

}
