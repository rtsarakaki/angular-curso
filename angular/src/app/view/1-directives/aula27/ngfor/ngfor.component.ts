import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-for',
  templateUrl: './ngfor.component.html',
  styleUrls: ['./ngfor.component.scss']
})
export class NgForComponent implements OnInit {
  movies: string[] = ['De volta para o futuro', 'Forest Gump', 'A Vida é Bela'];
  showMovies: boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

  changeStateMoviesFrame() {
    this.showMovies = !this.showMovies;
  }
}
