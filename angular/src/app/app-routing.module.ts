import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '', pathMatch: 'full', redirectTo: ''
  },
  {
    path: 'basics',
    loadChildren: () => import('./view/0-basics/basics.module').then(m => m.BasicsModule)
  },
  {
    path: 'directives',
    loadChildren: () => import('./view/1-directives/directives.module').then(m => m.DirectivesModule)
  },
  {
    path: 'services',
    loadChildren: () => import('./view/2-services/services.module').then(m => m.ServicesModule)
  },
  {
    path: 'pipes',
    loadChildren: () => import('./view/3-pipes/pipes.module').then(m => m.PipesModule)
  },
  {
    path: 'framework',
    loadChildren: () => import('./view/framework/framework.module').then(m => m.FrameworkModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
