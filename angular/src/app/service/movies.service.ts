import { EventEmitter, Injectable, Output } from '@angular/core';

import { LogService } from './log.service';

@Injectable({
  providedIn: 'root',
})
export class MoviesService {

  movieCreated = new EventEmitter<string>();
  static movieCreatedGlobal = new EventEmitter<string>();

  private movies: string[] = [];

  getMovies(): string[] {
    this._logService.consoleLog('Obtendo lista de filmes.');
    return this.movies;
  }

  addMovie(movie: string) {
    this._logService.consoleLog(`Criando um novo filme ${movie}.`);
    this.movies.push(movie);
    this.movieCreated.emit(movie);
    MoviesService.movieCreatedGlobal.emit(movie);
  }

  constructor(private _logService: LogService) {
    console.log('MoviesService');
  }
}
