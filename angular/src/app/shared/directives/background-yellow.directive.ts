import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[dirBackgroundYellow]'
})
export class BackgroundYellowDirective {

  constructor(_elementRef: ElementRef) {
    _elementRef.nativeElement.style.backgroundColor = 'yellow';
  }
}
