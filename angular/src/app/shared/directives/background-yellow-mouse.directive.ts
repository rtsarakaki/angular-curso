import { Directive, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[dirBackgroundYellowMouse]',
})

export class BackgroundYellowMouseDirective {
  @HostListener('mouseenter') onMouseOver() {
    /*this._renderer.setStyle(
      this._elementRef.nativeElement,
      'background-color',
      'yellow'
    );*/
    this.setColor = 'yellow';
  }

  @HostListener('mouseleave') onMouseLeave() {
    /*this._renderer.setStyle(
      this._elementRef.nativeElement,
      'background-color',
      'transparent'
    );*/
    this.setColor = 'transparent';
  }

  //@HostBinding('style.backgroundColor') backgroundColor!: string;
  @HostBinding('style.backgroundColor')
  get setColor() {
    return this.backgroudColor;
  }
  set setColor(color: string) {
    this.backgroudColor = color;
  }

  private backgroudColor!: string;

  constructor() {}
}
