import { Directive, HostListener, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[dirHighlight]',
})

export class HighlightDirective {
  @HostListener('mouseenter') onMouseOver() {
    /*this._renderer.setStyle(
      this._elementRef.nativeElement,
      'background-color',
      'yellow'
    );*/
    this.setColor = this.highlightColor;
  }

  @HostListener('mouseleave') onMouseLeave() {
    /*this._renderer.setStyle(
      this._elementRef.nativeElement,
      'background-color',
      'transparent'
    );*/
    this.setColor = this.defaultColor;
  }

  //@HostBinding('style.backgroundColor') backgroundColor!: string;
  @HostBinding('style.backgroundColor')
  get setColor() {
    return this.backgroudColor;
  }
  set setColor(color: string) {
    this.backgroudColor = color;
  }

  private backgroudColor!: string;

  @Input() defaultColor: string = 'transparent';
  @Input('dirHighlight') highlightColor: string = 'yellow';

  constructor() {}

  ngOnInit() {
    this.setColor = this.defaultColor;
  }
}
