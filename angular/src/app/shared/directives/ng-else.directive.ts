import { Directive, Input, TemplateRef, ViewContainerRef, ElementRef } from '@angular/core';

@Directive({
  selector: '[myNgElse]',
})
export class NgElseDirective {
  @Input('myNgElse')
  set ngElse(condition: boolean) {
    if (!condition) {
      this._viewContainerRef.createEmbeddedView(this._templateRef);
    }
    else {
      this._viewContainerRef.clear();
    }
  }

  constructor(
    private _templateRef: TemplateRef<any>,
    private _viewContainerRef: ViewContainerRef
  ) {

  }

  // constructor(_elementRef: ElementRef) {
  //   _elementRef.nativeElement.style.backgroundColor = 'yellow';
  // }
}
