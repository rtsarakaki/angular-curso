import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { BackgroundYellowDirective } from '../shared/directives/background-yellow.directive';
import { MaterialModule } from '../shared/material/material.module';
import { MasterContentComponent } from './controls/master-content/master-content.component';
import { BackgroundYellowMouseDirective } from './directives/background-yellow-mouse.directive';
import { HighlightDirective } from './directives/highlight.directive';
import { NgElseDirective } from './directives/ng-else.directive';
import { CammelCasePipe } from './pipes/cammel-case.pipe';
import { ArrayFilterPipe } from './pipes/array-filter.pipe';
import { ArrayFilterImpurePipe } from './pipes/array-filter-impure.pipe';
import { InjectHTMLDirective } from './directives/inject-html.directive';

@NgModule({
  declarations: [
    BackgroundYellowDirective,
    BackgroundYellowMouseDirective,
    HighlightDirective,
    NgElseDirective,
    MasterContentComponent,
    CammelCasePipe,
    ArrayFilterPipe,
    ArrayFilterImpurePipe,
    InjectHTMLDirective
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    BackgroundYellowDirective,
    BackgroundYellowMouseDirective,
    HighlightDirective,
    NgElseDirective,
    MasterContentComponent,
    CammelCasePipe,
    ArrayFilterPipe,
    ArrayFilterImpurePipe
  ]
})
export class SharedModule { }
