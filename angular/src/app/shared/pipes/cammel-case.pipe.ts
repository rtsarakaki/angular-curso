import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rtLowerCase'
})
export class CammelCasePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    let val = <string>value;
    return val.toLowerCase();
  }

}
