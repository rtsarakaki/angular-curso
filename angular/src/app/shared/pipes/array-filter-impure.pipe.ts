import { ArrayFilterPipe } from './array-filter.pipe';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rtArrayFilterImpure',
  pure: false
})
export class ArrayFilterImpurePipe extends ArrayFilterPipe {

}
