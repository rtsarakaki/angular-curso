import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rtArrayFilter'
})
export class ArrayFilterPipe implements PipeTransform {

  transform(value: Array<string>, ...args: string[]): Array<string> {
    if (value.length === 0 || args === undefined) {
      return value;
    }

    let filter = args[0].toLocaleLowerCase();
    return value.filter(
      v => v.toLocaleLowerCase().indexOf(filter) != -1
    );
  }

}
